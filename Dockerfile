# Specify parent image. Please select a fixed tag here.
FROM registry.git.rwth-aachen.de/jupyter/profiles/rwth-minimal-2021-ss:latest
# .. Or update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml
# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y
#
#RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager@3.0.0
#
#RUN jupyter labextension install jupyter-matplotlib@0.9.0
#
#RUN jupyter labextension install jupyterlab-datawidgets@7.0.0
#
#RUN jupyter labextension install ipytree@0.2.1
#
#RUN jupyter labextension install k3d@2.9.3
#
RUN jupyter labextension install ipyregulartable@0.2.0

RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager

RUN jupyter labextension install k3d